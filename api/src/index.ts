// import {PrismaClient} from "@prisma/client"
//
// const prisma = new PrismaClient()
//
// async function main() {
//     await prisma.user.create({
//         data: {
//             name: "Alice",
//             email: "alice@prisma.iio",
//             posts: {
//                 create: {title: "Hello World"},
//             },
//             profile: {
//                 create: {bio: "I like turtles"}
//             }
//         }
//     })
//     const allUsers = await prisma.user.findMany({
//         select: {
//             email: true,
//             name: true,
//             posts: {
//                 where: {published: true},
//                 select: {
//                     content: true,
//                     title: true,
//                 }
//             },
//             profile: {
//                 select: {
//                     bio: true
//                 }
//             }
//         },
//     })
//     console.dir(allUsers, {depth: null})
// }
//
// main()
//     .catch(e => {
//         throw e
//     })
//     .finally(async () => {
//         await prisma.disconnect()
//     })
