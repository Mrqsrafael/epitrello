import express, {NextFunction, Request, Response} from "express";
import compression from "compression"; // compresses requests
import bodyParser from "body-parser";
import lusca from "lusca";
import "./util/secrets";

import usersRouter from "./services/user/routes";
import boardRouter from "./services/board/routes";
import authRouter from "./services/auth/routes";
import listRouter from "./services/list/routes";
import logger from "./util/logger";
import {Code} from "./types";

// Create Express server
const app = express();

// Express configuration
app.set("port", process.env.PORT || 3000);
app.use(compression());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(lusca.xframe("SAMEORIGIN"));
app.use(lusca.xssProtection(true));
app.use((req: Request, res: Response, next: NextFunction) => {
    logger.info(req.originalUrl);
    return next();
});

app.get("/", (_req, res) => {
    return res.status(Code.OK).json({"message": "Bonjour"});
});

app.use("/", usersRouter);
app.use("/", authRouter);

app.use("/", boardRouter);

app.use("/", listRouter);

export default app;
