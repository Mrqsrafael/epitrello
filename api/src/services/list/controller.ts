import {PrismaClient} from "@prisma/client";

const prisma = new PrismaClient();

export async function getAll(boardID: string) {
    if (!boardID) {
        return await prisma.list.findMany();
    } else {
        return await prisma.list.findMany({
            where: {
                Board: {
                    id: Number(boardID)
                },
            },
        });
    }
}

export async function getByID(id: number) {
    return await prisma.list.findOne({
        where: {
            id: id
        }
    });
}

export async function create(title: string, id: number) {
     return await prisma.list.create({
        data: {
            title,
            Board: {
                connect: { id: id  }
            }
        },
     });
}

export async function update(title: string, id: number) {
    return await prisma.list.update({
        where: {
            id: id
        },
        data: {
            title
        }
    });
}

export async function deleteByID(id: number) {
    return await prisma.list.delete({
        where: {
            id: id
        }
    });
}