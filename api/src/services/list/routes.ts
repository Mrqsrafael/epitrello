import {Router} from "express";

import publicRouter from "./public/routes";
import privateRouter from "./private/routes";

const router = Router();

router.use("/lists", publicRouter);
router.use("/admin/lists", privateRouter);

export default router;