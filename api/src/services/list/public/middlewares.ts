import {Request, Response} from "express";
import * as listController from "../controller";
import { Code } from "../../../types";

export async function getAll(req: Request, res: Response): Promise<Response> {
    const { boardID } = req.body;

    const lists = await listController.getAll(boardID);
    return res.status(Code.OK).json({ lists });
}

export async function getList(req: Request, res: Response): Promise<Response> {
    const { id } = req.body;

    const lists = await listController.getByID(Number(id));
    return res.status(Code.OK).json({ lists });
}

export async function createList(req: Request, res: Response): Promise<Response> {
    const { title, id } = req.body;

    const list = await listController.create(title, Number(id));
    return res.status(Code.OK).json({ list });
}

export async function updateList(req: Request, res: Response): Promise<Response> {
    const { title, id } = req.body;

    const list = await listController.update(title, Number(id));
    return res.status(Code.OK).json({ list });
}

export async function deleteList(req: Request, res: Response): Promise<Response> {
    const { id } = req.body;

    const list = await listController.deleteByID(Number(id));
    return res.status(Code.OK).json({ list });
}

