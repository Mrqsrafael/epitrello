import { Router } from "express";

import {createList, deleteList, getAll, getList, updateList} from "./middlewares";

import { userAuthMiddlewares } from "../../auth/middlewares";


const router = Router();

router.get("/all", [...userAuthMiddlewares, getAll]);
router.get("", [...userAuthMiddlewares, getList]);
router.post("", [...userAuthMiddlewares, createList]);
router.put("",[...userAuthMiddlewares, updateList]);
router.delete("", [...userAuthMiddlewares, deleteList]);

export default router;
