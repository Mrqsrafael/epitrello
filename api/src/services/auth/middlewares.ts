import {NextFunction, Request, Response} from "express";
import {Bearer} from "permit";
import * as jwt from "jsonwebtoken";

import {Code, User} from "../../types";
import {create, getOneID, getOneUsername} from "../user/controller";
import {generateAuthToken} from "./auth";
import logger from "../../util/logger";

const jwtKey = process.env["JWT_KEY"];
const permit = new Bearer({
    query: "access_token", // Also allow an `?access_token=` query parameter.
});

export function extractToken(req: Request, res: Response, next: NextFunction) {
    if (!jwtKey)
        return new Error("No key");
    // Try to find the bearer token in the request.
    const token = permit.check(req);

    // No token found, so ask for authentication.
    if (!token) {
        logger.debug("extractToken: Token is null");
        permit.fail(res);
        return next(new Error("Authentication required!"));
    }
    req.token = token;
    return next();
}

export async function authenticate(req: Request, res: Response, next: NextFunction) {
    const payload = jwt.decode(req.token);

    if (!payload) {
        logger.debug("Authenticate: Payload is null");
        permit.fail(res);
        return next(new Error("Authentication required!"));
    }

    // @ts-ignore
    const id = payload?.id;

    const user: User | null = await getOneID(id);
    if (!user) {
        permit.fail(res);
        return next(new Error("Authentication invalid!"));
    }
    req.user = user;
    return next();

    // Perform your authentication logic however you'd like...
    // db.users.findByToken(token, (err: Error, user) => {
    //     if (err) return next(err)
    //
    //     // No user found, so their token was invalid.
    //     if (!user) {
    //         permit.fail(res)
    //         return next(new Error(`Authentication invalid!`))
    //     }
    //
    //     // Authentication succeeded, save the context and proceed...
    //     req.user = user
    //     next()
    // })
}

export async function register(req: Request, res: Response) {
    const {email, username, password} = req.body;

    if (!(email && username && password))
        return res.status(Code.BAD_REQUEST);
    const existingUser = await getOneUsername(username);
    if (existingUser)
        return res.status(Code.CONFLICT);
    const newUser = await create(email, username, password);
    return res.status(Code.CREATED).json({token: generateAuthToken(newUser)});
}

export async function login(req: Request, res: Response) {
    const {username, password} = req.body;

    if (!(username && password)) {
        logger.debug("Login: Missing username or password");
        return res.status(Code.BAD_REQUEST);
    }
    const user = await getOneUsername(username);
    if (!user) {
        logger.debug("Login: User not found");
        return res.status(Code.NOT_FOUND);
    }
    logger.debug("Login: Sending token");
    return res.status(Code.OK).json({"token": generateAuthToken(user)});
}

export async function adminRequired(req: Request, res: Response, next: NextFunction) {
    const {memberType} = req.user;

    if (memberType !== "admin") {
        return res.sendStatus(Code.UNAUTHORIZED);
    }
    return next();
}

export const userAuthMiddlewares = [extractToken, authenticate];

export const adminAuthMiddlewares = [extractToken, authenticate, adminRequired];
