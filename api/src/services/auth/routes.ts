import {Request, Response, Router} from "express";

import {authenticate, extractToken, login, register} from "./middlewares";
import logger from "../../util/logger";

const router = Router();

router.get("/protected",
    extractToken,
    authenticate,
    async (req: Request, res: Response) => {
        const {user} = req;
        logger.debug(`Protected: user: ${user.username} ${user.Profile?.id}`);
        res.send({"message": "OK"});
    });

router.post("/register", [register]);
router.post("/login", [login]);

export default router;
