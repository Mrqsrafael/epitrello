import {Router} from "express";

import publicRouter from "./public/routes";
import privateRouter from "./private/routes";

const router = Router();

router.use(publicRouter);
router.use(privateRouter);

export default router;
