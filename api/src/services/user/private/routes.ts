import {Router} from "express";

import {createUser, deleteUser, getUsers} from "./middlewares";
import {adminAuthMiddlewares} from "../../auth/middlewares";

const router = Router();

router.get("/users", [...adminAuthMiddlewares, getUsers]);

router.post("/user", [...adminAuthMiddlewares, createUser]);

router.delete('/user', [...adminAuthMiddlewares, deleteUser]);

export default router;
