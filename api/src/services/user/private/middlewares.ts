import {Request, Response} from "express";
import * as controller from "../controller";
import {Code} from "../../../types";
import logger from "../../../util/logger";


export async function getUsers(req: Request, res: Response) {
    const users = await controller.getAll();
    return res.status(Code.OK).json({users});
}


export async function deleteUser(req: Request, res: Response) {
    let {username, id, email} = req.body;

    if (!(username || id || email)) {
        logger.debug(`deleteUser: missing fields: username:${username} id:${id} email:${email}`)
        return res.sendStatus(Code.BAD_REQUEST);
    }
    if (id) {
        try {
            const deletedUser = await controller.deleteWithID(id);
            if (!deletedUser) {
                return res.status(Code.NOT_FOUND);
            }
            return res.sendStatus(Code.OK)
        } catch (err) {
            logger.error(err)
            return res.json(err).status(Code.INTERNAL_SERVER_ERROR)
        }
    }
    return res.sendStatus(Code.NOT_FOUND);
}

export async function createUser(req: Request, res: Response) {
    return res.sendStatus(Code.NOT_IMPLEMENTED);
}

