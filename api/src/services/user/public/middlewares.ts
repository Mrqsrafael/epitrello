import {Request, Response} from "express";
import * as controller from "../controller";
import logger from "../../../util/logger";
import {Code, User} from "../../../types";

export async function getUser(req: Request, res: Response) {
    const {username, id} = req.body;
    let user: User | null = null;

    if (!username && !id)
        return res.sendStatus(Code.BAD_REQUEST);
    try {
        if (id) {
            user = await controller.getOneID(id);
        } else if (username) {
            if (username === "me") {
                user = req.user;
            } else {
                user = await controller.getOneUsername(username);
            }
        }
        if (!user) {
            return res.sendStatus(Code.NOT_FOUND)
        }
        delete user.password;
        return res.status(Code.OK).json({...user});
    } catch (err) {
        logger.error(err);
        return res.sendStatus(Code.INTERNAL_SERVER_ERROR);
    }
}

export async function updateUser(req: Request, res: Response) {
    return res.sendStatus(Code.NOT_IMPLEMENTED);
}

export async function deleteMe(request: Request, res: Response) {
    return res.sendStatus(Code.NOT_IMPLEMENTED);
}
