import {Router} from "express";

import {userAuthMiddlewares} from "../../auth/middlewares";
import {deleteMe, getUser, updateUser} from "./middlewares";

const router = Router();


router.get('/user', [...userAuthMiddlewares, getUser]);

router.put("/user", [...userAuthMiddlewares, updateUser]);

router.post("/user/me/delete", [...userAuthMiddlewares, deleteMe]);

export default router;
