import {PrismaClient} from "@prisma/client";
import {User} from "../../types";

const prisma = new PrismaClient();

export async function getAll(): Promise<Record<string, any>[]> {
    return await prisma.user.findMany();
}

export async function getOneUsername(username: string): Promise<User | null> {
    return await prisma.user.findOne({
        where: {
            username
        },
        include: {Profile: true}
    });
}

export async function getOneID(id: string): Promise<User | null> {
    return await prisma.user.findOne({
        where: {
            id: Number(id)
        },
        include: {Profile: true}
    });
}

export async function getOneEmail(email: string): Promise<User | null> {
    return await prisma.user.findOne({
        where: {
            email
        },
        include: {Profile: true}
    });
}

export async function create(email: string, username: string, password: string): Promise<User> {
    return await prisma.user.create({
        data: {
            email,
            username,
            password,
            Profile: {
                create: {}
            },
        },
        include: {Profile: true}
    });
}

export async function deleteWithID(id: string): Promise<User> {
    return await prisma.user.delete({
        where: {
            id: Number(id)
        },
        include: {Profile: true}
    });
}
