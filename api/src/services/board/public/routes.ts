import { Router } from "express";

import {getAll, getBoard, createBoard, updateBoard, removeBoard, getBoards} from "./middlewares";

import { userAuthMiddlewares } from "../../auth/middlewares";


const router = Router();

router.get("/admin", [...userAuthMiddlewares, getAll]);
router.get("/all", [...userAuthMiddlewares, getBoards]);
//router.get("/addMember", [...userAuthMiddlewares, ]);
router.get("", [...userAuthMiddlewares, getBoard]);
router.post("", [...userAuthMiddlewares, createBoard]);
router.put("",[...userAuthMiddlewares, updateBoard]);
router.delete("", [...userAuthMiddlewares, removeBoard ]);

export default router;
