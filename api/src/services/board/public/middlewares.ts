import {Request, Response} from "express";
import * as boardController from "../controller";
import { Code } from "../../../types";

export async function createBoard(req: Request, res: Response): Promise<Response> {
    const { user } = req;
    const { name } = req.body;
    if (name != undefined && user) {
        try {
            // @ts-ignore
            const board = await boardController.create(name, user.Profile.id);
            return res.status(Code.OK).json({board});
        } catch {
            res.statusMessage = "internal server error";
            return res.status(Code.INTERNAL_SERVER_ERROR);
        }
    } else {
        res.statusMessage = "The name of the board was undefined";
        return res.status(Code.BAD_REQUEST);
    }
}

export async function getAll(req: Request, res: Response): Promise<Response> {
    const boards = await boardController.getAll();
    return res.status(Code.OK).json({boards});
}

export async function removeBoard(req: Request, res: Response): Promise<Response> {
    const { user } = req;
    const { boardID } = req.body;

    // @ts-ignore
    const board = await boardController.remove(Number(boardID), user.Profile.id);
    if (board != null) {
        return res.status(Code.OK).json( { board} );
    } else {
        return res.status(Code.BAD_REQUEST);
    }
}

export async function getBoards(req: Request, res: Response): Promise<Response> {
    const { user } = req;

    // @ts-ignore
    const boards = await boardController.getUserBoards(user.Profile.id);
    if (boards != null) {
        return res.status(Code.OK).json( { boards } );
    } else {
        return res.status(Code.BAD_REQUEST);
    }
}

export async function getBoard(req: Request, res: Response): Promise<Response> {
    const { user } = req;
    const { boardID } = req.body;

    // @ts-ignore
    const board = await boardController.getOne(Number(boardID), user.Profile.id);
    if (board != null) {
        return res.status(Code.OK).json( { board } );
    } else {
        return res.status(Code.BAD_REQUEST);
    }
}

export async function updateBoard(req: Request, res: Response): Promise<Response> {
    const { user } = req;
    const { boardID, name } = req.body;

    // @ts-ignore
    const updatedBoard = await boardController.update(Number(boardID), user.Profile.id, name);
    if (updatedBoard != null) {
        return res.status(Code.OK).json( { updatedBoard} );
    } else {
        return res.status(Code.BAD_REQUEST);
    }
}

/*
export async function addMember(req: Request, res: Response): Promise<Response> {
    const { boardID, userID } = req.body;

    // @ts-ignore
    const updatedBoard = await boardController.addMember(boardID, userID);
    if (updatedBoard != null) {
        return res.status(Code.OK).json( { updatedBoard} );
    } else {
        return res.status(Code.BAD_REQUEST);
    }
}
*/
