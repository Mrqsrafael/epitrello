import {Router} from "express";

import publicRouter from "./public/routes";
import privateRouter from "./private/routes";

const router = Router();

router.use("/boards", publicRouter);
router.use("/admin/boards", privateRouter);

export default router;