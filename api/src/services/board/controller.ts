import {PrismaClient} from "@prisma/client";

const prisma = new PrismaClient();

export async function getAll() {
    return await prisma.board.findMany({
        include: {
            List: {}
        }
    });
}

export async function create(name: string, id: number){
     return await prisma.board.create({
        data: {
            name,
            Profile: {
                connect: { id: id  }
            }
        },
     });
}

export async function remove(id: number, userID: number) {
    const board = await prisma.board.findMany({
            where: {
                Profile: { id: userID },
                id: id,
            }
        });
    if (board.length > 0) {
        return await prisma.board.delete({
            where: {
                id: id
            },
            include: {
                List: {
                }
            }
        });
    } else {
        return [];
    }
}

export async function getUserBoards(userID: number) {
    return await prisma.board.findMany({
        where: {
            Profile: { id: userID }
        }
    });
}

export async function getOne(id: number, userID: number) {
    return await prisma.board.findMany({
        where: {
            id: id,
            Profile: { id: userID }
        }
    });
}

export async function update(id: number, userID: number, name: string) {
    const board = await prisma.board.findMany({
        where: {
            Profile: { id: userID },
            id: id,
        }
    });
    if (board.length > 0) {
        return await prisma.board.update({
            where: {
                id: id,
            },
            data: {
                name: name,
            },
        });
    } else {
        return [];
    }
}

/*export async function addMember(boardID: number, userID: number) {
    const board = await prisma.board.findMany({
        where: {
            id: boardID,
        }
    });
    if (board.length > 0) {
        return await prisma.board.update({
            where: {
                id: boardID,
            },
            data: {
                memberID: xxx
            },
        });
    } else {
        return [];
    }
}*/
