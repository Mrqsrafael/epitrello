import request from "supertest";
import app from "../src/app";

describe("GET /users", () => {
    it("should return 200 OK", () => {
        return request(app).get("/users")
            .expect(200);
    });
});

// describe("GET /register", () => {
//     it("should return 200 OK", () => {
//         return request(app).get("/users")
//             .expect(200);
//     });
// });
