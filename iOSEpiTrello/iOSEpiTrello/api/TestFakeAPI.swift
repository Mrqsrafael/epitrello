//
//  TestFakeAPI.swift
//  iOSEpiTrello
//
//  Created by Maxime Gernath on 08/05/2020.
//  Copyright © 2020 MaximeCorp. All rights reserved.
//

import Foundation
import UIKit

class FakeAPIClient {
    func getData() ->[GroupItem] {
        var array: [GroupItem] = [GroupItem]()
        let item1 = GroupItem(image: UIImage.init(named: "Card1"), text:"Card1")
        let item2 = GroupItem(image: UIImage.init(named: "Card2"), text:"Card2")
        let item3 = GroupItem(image: UIImage.init(named: "Card3"), text:"Card3")
        let item4 = GroupItem(image: UIImage.init(named: "Card4"), text:"Card4")
        let item5 = GroupItem(image: UIImage.init(named: "Card5"), text:"Card5")
        array.append(item1)
        array.append(item2)
        array.append(item3)
        array.append(item4)
        array.append(item5)
        return array
   }
}
