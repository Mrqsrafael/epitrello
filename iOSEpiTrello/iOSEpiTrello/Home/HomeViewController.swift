//
//  HomeViewController.swift
//  iOSEpiTrello
//
//  Created by Maxime Gernath on 08/05/2020.
//  Copyright © 2020 MaximeCorp. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {

    @IBOutlet weak var tableView:UITableView!
    var arrayOfItems:[GroupItem] = [GroupItem]()

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        arrayOfItems = FakeAPIClient().getData()
        tableView.reloadData()
    }
}

extension HomeViewController: UITableViewDataSource, UITableViewDelegate {

   func tableView(_ tableView: UITableView, numberOfRowsInSection    section: Int) -> Int {
        return arrayOfItems.count
   }

   func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      if let cell = tableView.dequeueReusableCell(withIdentifier: "GroupCellIdentifier") as? GroupCell {
         cell.configureCell(item: arrayOfItems[indexPath.row])
         return cell
      }
      return UITableViewCell()
   }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       return 200
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       tableView.deselectRow(at: indexPath, animated: true)
       let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
       if let dataPresentingViewController = storyBoard.instantiateViewController(withIdentifier: "BoardViewControllerIdentifier") as? BoardViewController {
            dataPresentingViewController.modalPresentationStyle = .fullScreen
            self.present(dataPresentingViewController, animated: false, completion: nil)
            dataPresentingViewController.dataLabel.text = arrayOfItems[indexPath.row].text
       }
    }
}
