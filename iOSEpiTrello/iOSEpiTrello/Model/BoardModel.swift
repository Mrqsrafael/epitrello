//
//  BoardModel.swift
//  iOSEpiTrello
//
//  Created by Maxime Gernath on 09/05/2020.
//  Copyright © 2020 MaximeCorp. All rights reserved.
//

import Foundation

class Board: Codable {
    
    var title: String
    var items: [String]
    
    init(title: String, items: [String]) {
        self.title = title
        self.items = items
    }
}
