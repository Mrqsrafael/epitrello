//
//  BoardViewController.swift
//  iOSEpiTrello
//
//  Created by Maxime Gernath on 08/05/2020.
//  Copyright © 2020 MaximeCorp. All rights reserved.
//

import Foundation
import UIKit
class BoardViewController: UIViewController {
   @IBOutlet weak var dataLabel:UILabel!

    @IBAction func dismissPressed(_ sender:Any) {
       self.dismiss(animated: true, completion: nil)
    }
}
