//
//  GroupTableView.swift
//  iOSEpiTrello
//
//  Created by Maxime Gernath on 08/05/2020.
//  Copyright © 2020 MaximeCorp. All rights reserved.
//

import UIKit
import Foundation

class GroupCell: UITableViewCell {
    @IBOutlet weak var cellLabel:UILabel!
    @IBOutlet weak var cellImage:UIImageView!
    
    func configureCell(item:GroupItem) {
       cellLabel.text = item.text
       cellImage.image = item.image
    }

}
