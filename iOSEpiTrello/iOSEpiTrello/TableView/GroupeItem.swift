//
//  GroupeItem.swift
//  iOSEpiTrello
//
//  Created by Maxime Gernath on 08/05/2020.
//  Copyright © 2020 MaximeCorp. All rights reserved.
//

import Foundation
import UIKit

struct GroupItem {
   let image: UIImage?
   let text: String?
}
