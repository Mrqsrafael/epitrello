//
//  UIFont.swift
//  EpiTrelloiOS
//
//  Created by Maxime Gernath on 30/04/2020.
//  Copyright © 2020 MaximeCorp. All rights reserved.
//

import UIKit

private struct Default {
    static let font = UIFont.systemFont(ofSize: 16)
}

extension UIFont {
    public class var Regular: UIFont {
        return UIFont(name: "PingFangTC-Regular", size: 16) ?? Default.font
    }
    public class var SemiBold: UIFont {
        return UIFont(name: "PingFangTC-SemiBold", size: 16) ?? Default.font
    }
    public class var Title: UIFont {
        return UIFont(name: "PingFangTC-SemiBold", size: 22) ?? Default.font
    }
}
